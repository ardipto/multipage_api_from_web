import 'package:flutter/material.dart';
import 'package:multipage/page1.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() {
  runApp(MaterialApp(
    home: Dashboard(),
  ));
}

class Dashboard extends StatefulWidget {
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List posts;

  Future<bool> _getPosts() async {
    String serviceUrl = 'https://jsonplaceholder.typicode.com/posts';
    var response = await http.get(Uri.parse(serviceUrl));
    setState(() {
      posts = json.decode(response.body.toString());
      print(posts[0]);
    });
    return true;
  }

  @override
  void initState() {
    super.initState();
    this._getPosts();
  }

  @override
  Widget build(BuildContext context) {
    // List people = [
    //   {"name": "Dipto", "email": "ardipta82@gmail.com"},
    //   {"name": "sfdg", "email": "xdfg@gmail.com"},
    //   {"name": "cvvc", "email": "jhg@gmail.com"},
    //   {"name": "Dipsreto", "email": "sfdg@gmail.com"},
    //   {"name": "Ddcgvipto", "email": "sfdg@gmail.com"},
    //   {"name": "fdgf", "email": "sfdgc@gmail.com"},
    //   {"name": "cxv", "email": "sdfg@gmail.com"},
    //   {"name": "fdsger", "email": "ffdfg@gmail.com"},
    //   {"name": "grsdg", "email": "adfg@gmail.com"},
    //   {"name": "fsdg", "email": "cxh@gmail.com"},
    //   {"name": "sdfgv", "email": "hjjcfg@gmail.com"},
    //   {"name": "gfhfg", "email": "ardgrdgipta82@gmail.com"},
    //   {"name": "vdcfgfd", "email": "fdg@gmail.com"},
    //   {"name": "zcxg", "email": "ardipsdfgta82@gmail.com"},
    //   {"name": "wad", "email": "sdfg@gmail.com"},
    // ];
    return Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
          centerTitle: true,
        ),
        body: ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: posts.length == null ? 0 : posts.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              title: Text(posts[index]['title']),
              onTap: () {
                Route route = MaterialPageRoute(
                    builder: (context) => PageOne(posts[index]));
                Navigator.push(context, route);
              },
              // title: Text(people[index]['name']), //list send to another page
              // subtitle: Text(people[index]['email']),
              // onTap: () {
              //   Route route = MaterialPageRoute(
              //       builder: (context) => PageOne(people[index]));
              //   Navigator.push(context, route);
              // },
            );
          },
        ));
  }
}
